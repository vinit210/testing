import { Component } from '@angular/core';
import { ProductService } from './product.service'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  products = [ ]
  constructor(private productService : ProductService) { }

  ngOnInit(): void {
    this.productService
    .getProduct()
    .subscribe(response => {
      if(response['status'] == 'success'){
        this.products = response['data']
      }else{
        alert('error')
      }
    })
  }
}
